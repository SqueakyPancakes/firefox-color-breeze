![Firefox color breeze screenshot](/Screenshot_20180930_154447.png?raw=true)

# Firefox Color Breeze

This uses the firefox color plugin to create a better KDE Breeze theme for firefox.


## Installation

Step 1.
Install firefox color https://testpilot.firefox.com/experiments/color

Step 2.
click here https://color.firefox.com/?theme=XQAAAALVAAAAAAAAAABBKYhm849SCiazH1KEGccwS-xNVAWBvR_gsLCeVs7H47464nRGJfhdZYnyTyTiEBwnD4ERqZ3 and click apply.

Step 3.
There is no step 3.